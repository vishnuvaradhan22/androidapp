Android Application



Environment setup(IDE):
Requirement:
1.Java (openjdk 1.6 and above)

Steps to follow:
1. Download Android Studio - Development environment for Android App
    https://developer.android.com/sdk/installing/studio.html

2. Run the studio.sh file to open the IDE

3. Set the build path for java and Android SDK in IDE, File->Project Structure

4. Clone the project from Bitbucket

5. Check whether twitter4j library is added as dependencies. File->Project Structure-> Modules-> Dependencies

6. Update the SDK with SDK Manager in IDE

7. Create a new Android Virtual Device(AVD) with the following configuration as Minimum configuration
        Target API: API level 19
        RAM         : 1024 Mb
        Memory      : 5000 Mb

8. Start the Android Virtual Device from AVD Manager. This will open the Target mobile simulator.

9. Run the project, Run-> Run 'app' or click Run button

10. Select the active simulator to install and run the app.



Instruction for Installing in Mobile:
1. Copy the app.apk file to the mobile
2. Run app.apk to install the app