package com.example.etisalat.app;

import android.accounts.Account;
import android.accounts.AccountManager;
import android.accounts.AccountManagerCallback;
import android.accounts.AccountManagerFuture;
import android.content.Context;
import android.content.Intent;
import android.database.Cursor;
import android.os.Build;
import android.provider.CallLog;
import android.support.v7.app.ActionBarActivity;
import android.os.Bundle;
import android.telephony.TelephonyManager;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.TextView;



import java.util.Date;


public class MainActivity extends ActionBarActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

    }


    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.main, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        return super.onOptionsItemSelected(item);
    }
    public void dualSim(View view)
    {
        TelephonyInfo telephonyInfo = TelephonyInfo.getInstance(this);

        String imeiSIM1 = telephonyInfo.getImeiSIM1();
        String imeiSIM2 = telephonyInfo.getImeiSIM2();

        boolean isSIM1Ready = telephonyInfo.isSIM1Ready();
        boolean isSIM2Ready = telephonyInfo.isSIM2Ready();
        boolean isDualSIM = telephonyInfo.isDualSIM();
        TextView tv = (TextView) findViewById(R.id.textView);
        tv.setText(" IME1 : " + imeiSIM1 + "\n" +
                " IME2 : " + imeiSIM2 + "\n" +
                " IS DUAL SIM : " + isDualSIM + "\n" +
                " IS SIM1 READY : " + isSIM1Ready + "\n" +
                " IS SIM2 READY : " + isSIM2Ready + "\n");


    }

    public void printDeviceDetails(View view) {
        //Print Device Detials
        //TelephonyManager class is used to retrieve the SIM card information
        TelephonyManager  telephonyManager=(TelephonyManager)getSystemService(Context.TELEPHONY_SERVICE);
        StringBuilder deviceDetails = new StringBuilder("Device Details\n");

        //Build class is used to get basic device details.
        deviceDetails.append("Device : ");
        deviceDetails.append(Build.DEVICE);
        deviceDetails.append("\nManufacturer : ");
        deviceDetails.append(Build.MANUFACTURER);
        deviceDetails.append("\nModel : ");
        deviceDetails.append(Build.MODEL);
        deviceDetails.append("\nBrand : ");
        deviceDetails.append(Build.BRAND);
        deviceDetails.append("\nProduct : ");
        deviceDetails.append(Build.PRODUCT);
        deviceDetails.append("\nDevice Serial Number : ");
        deviceDetails.append(Build.SERIAL);
        deviceDetails.append("\nIMEI Number : ");
        deviceDetails.append(telephonyManager.getDeviceId());

        deviceDetails.append("\nSoftware Version :");
        deviceDetails.append(telephonyManager.getDeviceSoftwareVersion());
        int simState = telephonyManager.getSimState();
        switch (simState)
        {
            case (TelephonyManager.SIM_STATE_ABSENT): break;
            case (TelephonyManager.SIM_STATE_NETWORK_LOCKED): break;
            case (TelephonyManager.SIM_STATE_PIN_REQUIRED): break;
            case (TelephonyManager.SIM_STATE_PUK_REQUIRED): break;
            case (TelephonyManager.SIM_STATE_UNKNOWN): break;
            case (TelephonyManager.SIM_STATE_READY):
            {
                // Get the SIM country ISO code
                deviceDetails.append("\nSim Country :");
                deviceDetails.append(telephonyManager.getSimCountryIso());
                // Get the operator code of the active SIM (MCC + MNC)
                deviceDetails.append("\nSim Operator Code :");
                deviceDetails.append(telephonyManager.getSimOperator());
                // Get the name of the SIM operator
                deviceDetails.append("\nSim Operator Name :");
                deviceDetails.append(telephonyManager.getSimOperatorName());
                // -- Requires READ_PHONE_STATE uses-permission --
                // Get the SIM’s serial number
                deviceDetails.append("\nSim Serial :");
                deviceDetails.append(telephonyManager.getSimSerialNumber());
                deviceDetails.append("\nMobile Number :");
                deviceDetails.append(telephonyManager.getLine1Number());

            }
        }
        //Check whether device is in Roaming
        boolean isRoaming=telephonyManager.isNetworkRoaming();
        if(isRoaming)
            deviceDetails.append("\nRoaming Status : YES");
        else
            deviceDetails.append("\nRoaming Status : NO");

        //Print Phone type
        int phoneType=telephonyManager.getPhoneType();
        switch (phoneType)
        {
            case TelephonyManager.PHONE_TYPE_CDMA:
                deviceDetails.append("\nPhone Type : CDMA");
                break;
            case TelephonyManager.PHONE_TYPE_GSM :
                    deviceDetails.append("\nPhone Type : GSM");
                break;
            case TelephonyManager.PHONE_TYPE_NONE:
                deviceDetails.append("\nPhone Type : NOT AVAILABLE");
                break;
        }

        TextView textView = (TextView) findViewById(R.id.textView);
        textView.setText(deviceDetails.toString());
    }
    public void accountdetails(View view){
        //Display the user accounts stored in the device
        StringBuilder accountsDetails = new StringBuilder("Accounts Details\n");
        AccountManager accountMgr = (AccountManager) getSystemService(Context.ACCOUNT_SERVICE);
        Account[] deviceAccounts = accountMgr.getAccounts();
        for(Account myAccount : deviceAccounts) {
            accountsDetails.append("\nAccount Name : ");
            accountsDetails.append(myAccount.name);
            accountsDetails.append("\nAccount Type : ");
            accountsDetails.append(myAccount.type);
        }
        TextView textView = (TextView) findViewById(R.id.textView);
        textView.setText(accountsDetails.toString());
    }
    public void openActivity(View view)
    {
        Intent intent = new Intent(MainActivity.this, Twitter_authentication.class);
        startActivity(intent);
    }

}
